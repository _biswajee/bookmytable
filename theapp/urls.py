from django.conf.urls import url, re_path
from . import views


urlpatterns = [
    url('index/', views.index, name = 'index'),
    url('signup/', views.signup, name = 'signup'),
    url('signin/', views.signin, name = 'signin'),
    url('dashboard/', views.dashboard, name = 'Home - Dashboard'),
    url(r'^places/', views.places, name = 'Search places'),
    url(r'^search_hotels/', views.hotels, name = 'Search Hotels')
]


'''
admin - status
username: biswajee
password: alia@bhatt
email: roy.biswajeet161@gmail.com
'''
