from django.shortcuts import render
from .forms import UsrRegForm
from django.template import loader
from django.http import HttpResponse
# from flask import request
import requests
import json
# Create your views here.


def index(request):
    return render(request, 'webpages/index.html')

'''
def signup(request):
    return render(request, 'webpages/signup.html')
'''


def signin(request):
    return render(request, 'webpages/signin.html')


def dashboard(request):
    return render(request, 'webpages/dashboard.html')



def places(request):
    try:
        pick = request.GET.get('pick')
        drop = request.GET.get('drop')
        pick_locations = "https://developers.zomato.com/api/v2.1/locations?query={0}&lat=1&lon=1&count=10".format(pick)
        drop_locations = "https://developers.zomato.com/api/v2.1/locations?query={0}&lat=1&lon=1&count=10".format(drop)
        header = {"User-agent": "curl/7.43.0", "Accept": "application/json", "user_key": "048c6b1d49717431eee4c63c511203a3"}
        p_response = requests.get(pick_locations, headers=header)
        d_response = requests.get(drop_locations, headers=header)

        p_response_text = json.dumps(p_response.json())
        p_response_text = json.loads(p_response_text)


        d_response_text = json.dumps(d_response.json())
        d_response_text = json.loads(d_response_text)


        p_city_name = p_response_text['location_suggestions'][0]['city_name']
        p_country_name = p_response_text['location_suggestions'][0]['country_name']
        p_latitude = p_response_text['location_suggestions'][0]['latitude']
        p_longitude = p_response_text['location_suggestions'][0]['longitude']


        d_city_name = d_response_text['location_suggestions'][0]['city_name']
        d_country_name = d_response_text['location_suggestions'][0]['country_name']
        d_latitude = d_response_text['location_suggestions'][0]['latitude']
        d_longitude = d_response_text['location_suggestions'][0]['longitude']
        #template = "webpages/results.html"

        context = {
            'p_city_name': p_city_name,
            'p_country_name': p_country_name,
            'p_latitude': p_latitude,
            'p_longitude': p_longitude,
            'd_city_name': d_city_name,
            'd_country_name': d_country_name,
            'd_latitude': d_latitude,
            'd_longitude': d_longitude
            }

    except:
        context = {
            'p_city_name': "Please try again !",
            'p_country_name': "Please try again !",
            'p_latitude': "Please try again !",
            'p_longitude': "Please try again !",
            'd_city_name': "Please try again !",
            'd_country_name': "Please try again !",
            'd_latitude': "Please try again !",
            'd_longitude': "Please try again !"
        }

    print(context)

    return render(request, 'webpages/results.html', context)


def hotels(request):
    entity_id = request.GET.get('entity_id')
    entity_type = request.GET.get('entity_type')
    query = request.GET.get('query')
    try:
        hotels = "https://developers.zomato.com/api/v2.1/search?entity_id={0}&entity_type={1}&q={2}&collection_id=1".format(entity_id, entity_type, query)
        header = {"User-agent": "curl/7.43.0", "Accept": "application/json", "user_key": "048c6b1d49717431eee4c63c511203a3"}
        response = requests.get(hotels, headers=header)

        response_text = json.dumps(response.json())
        response_text = json.loads(response_text)
        #template = "webpages/results.html"
        context = {
            'image_1': response_text['restaurants'][0]['restaurant']['featured_image'],
            'location_1': response_text['restaurants'][0]['restaurant']['location']['address'],
            'image_2': response_text['restaurants'][1]['restaurant']['featured_image'],
            'location_2': response_text['restaurants'][1]['restaurant']['location']['address'],
            'image_3': response_text['restaurants'][2]['restaurant']['featured_image'],
            'location_3': response_text['restaurants'][2]['restaurant']['location']['address'],
            'image_4': response_text['restaurants'][3]['restaurant']['featured_image'],
            'location_4': response_text['restaurants'][3]['restaurant']['location']['address'],
            'image_1': response_text['restaurants'][4]['restaurant']['featured_image'],
            'location_1': response_text['restaurants'][4]['restaurant']['location']['address']
            }
        print(context)
        return render(request, 'webpages/hotel_results.html', context)

    except:
        return render(request, 'webpages/404.html')


def signup(request):
    usrRgFrm = UsrRegForm()
    return HttpResponse((loader.get_template('webpages/signup.html')).render({'form': usrRgFrm}, request))


def addUser(request):
        us = UsrRegForm(request.POST)
        if (us.is_valid()):
            usr = us.save(commit=False)
            if(usr.pss==request.POST.get('pss_chk', 'empty')):
                usr.save()
                return render(request, 'webpages/signup.html', {'form': UsrRegForm(), 'data': "Successfully Registered!!", 'col_code': '1'})
            else:
                return render(request, 'webpages/signup.html', {'form': UsrRegForm(), 'data': "Registration Unsuccessful!!", 'col_code': '0'})
        else:
            return render(request, 'webpages/signup.html', {'form': UsrRegForm(), 'data': "Registration Unsuccessful!!", 'col_code': '0'})
